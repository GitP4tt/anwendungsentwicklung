package Aktien;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class YahooFinance {
	
	public static double getPrice(String tickerSymbol) throws IOException{
		URL url = new URL("http://ichart.finance.yahoo.com/"
				+ "table.csv?s=" + tickerSymbol);
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		
		reader.readLine();
		String data = reader.readLine();
		String[] items = data.split(",");
		double price = Double.valueOf(items[items.length - 1]);
		
		return price;
				
	}

}
