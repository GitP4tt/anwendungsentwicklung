package Aktien;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class AssetValueManager {

	public static void main(String[] args) 
			throws IOException, InterruptedException, ExecutionException {
		
		long start = System.nanoTime();
		
		AbstractAssetValueCalculator calculator = 
				new ConcurrentAssetValueCalculator("portfolio.txt");
		
			double totalValue = calculator.getTotalValue();
			
			long end = System.nanoTime();
			
			System.out.printf("Total value of your portfolio is $%,.2f\n", totalValue);
			System.out.printf("Retrieval took %,.2f seconds\n", (end - start)/1.0e9);
	}

}
