package Aktien;

import java.io.IOException;

public class SequentialAssetValueCalculator extends AbstractAssetValueCalculator {

	public SequentialAssetValueCalculator(String fileName) throws IOException {
		super(fileName);
	}
	
	@Override
	public double getTotalValue() throws IOException {
		
		double totalValue = 0;
		for(String tickerSymbol : stockQuantitiyByTickerSymbol.keySet()) {
			
			int quantity = stockQuantitiyByTickerSymbol.get(tickerSymbol).intValue();
			totalValue += YahooFinance.getPrice(tickerSymbol) * quantity;
			}
		return totalValue;
		}
	}

