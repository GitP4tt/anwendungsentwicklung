package FortuneTeller;

import akka.actor.ActorRef;
import akka.actor.Actors;
import akka.dispatch.Future;

public class FortuneTellerClient {

	public static void main(String[] args) {
		
		ActorRef fortuneTeller = Actors.actorOf(FortuneTeller.class);
		fortuneTeller.start();
		
		Future<Object> answer = fortuneTeller.sendRequestReplyFuture("wolfgang"); //Synchroner aufruf
		System.out.println(answer.await().get());
		fortuneTeller.stop();
	}

}
