package FortuneTeller;

import akka.actor.UntypedActor;

public class FortuneTeller extends UntypedActor {

	@Override
	public void onReceive(Object person) throws Exception {

		String answer = "Hi " + person + "! You will be rich!";
		getContext().replySafe(answer);
	}

}
