package DeadLock;

public class LeftRightDeadlock {

	private final Object left = new Object();
	private final Object right = new Object();

	private final static int ITERATIONS = 10000;
	private final static int SLEEP_TIME = 100;

	public void leftRight() {
		
		synchronized (left) {
			synchronized (right) {
				try {
					Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {

				}
			}
		}
	}

	public void rightLeft() {
		
		synchronized (right) {
			synchronized (left) {

				try {
					Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {

				}
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LeftRightDeadlock deadLock = new LeftRightDeadlock();
		Thread tR2L = new Thread() {
			
			@Override
			public void run() {
				for (int i =0; i < LeftRightDeadlock.ITERATIONS; i++){
					System.out.println("leftRight, i = " + i);
					deadLock.leftRight();
				}
			}
		};
			
		
			Thread tL2R = new Thread() {
				
				@Override
				public void run() {
					for (int i = 0; i < LeftRightDeadlock.ITERATIONS; i++){
						System.out.println("RightLeft, i = " + i);
						deadLock.rightLeft();
					}
				}
			};
			
			tL2R.start();
			tR2L.start();
			
	}

}
