package counterDeadlock;

/**
 * Simple integer sequence generator. <br>
 * Used to demonstrate dead lock <br>
 * 
 * @author Wolfgang Funk
 *
 */
public class DeadLockSequence implements Sequence {

  private int value = 0;
  private String name;

  public DeadLockSequence(String name) {
    
    super();
    this.name = name;
  }
  
  @Override
  public String getName() {
    
    return name;
  }
  
  @Override
  /**
   * Potential dead lock here, if called by multiple threads.
   */
  public synchronized int getAndIncrement(Sequence other) {

    System.out.println("value = " + getCurrent() + " thread "
        + Thread.currentThread().getName());
    
    int otherValue = other.getCurrent();   
    // Do something more useful here with other value
    otherValue = otherValue * 42;
 
    return value++;
  }

  @Override
  public synchronized int getCurrent() {

    return value;
  }

}
