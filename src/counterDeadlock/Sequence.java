package counterDeadlock;

/**
 * Simple integer sequence generator.
 * 
 * @author Wolfgang Funk
 *
 */
public interface Sequence {

  /**
   * Returns current value of the sequence
   * and increments sequence value afterwards.
   * 
   * @param other other <code>Sequence</code> object.
   * 
   * @return current integer value
   */
  int getAndIncrement(Sequence other);
  
  /**
   * Returns current value of the sequence.
   * 
   * @return current integer value
   */
  int getCurrent();
  
  /**
   * Get sequence name. Should be unique.
   * 
   * @return given name of sequence
   */
  String getName();

}
