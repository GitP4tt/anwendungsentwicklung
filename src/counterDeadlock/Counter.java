package counterDeadlock;

import java.util.List;
import java.util.ArrayList;

public class Counter {

  public static void main(String[] args) {

    Sequence seq1 = new DeadLockSequence("seq1");
    Sequence seq2 = new DeadLockSequence("seq2");
    List<Sequence> lstSeq1 = new ArrayList<Sequence>();
    List<Sequence> lstSeq2 = new ArrayList<Sequence>();
    lstSeq1.add(seq1);
    lstSeq1.add(seq2);
    lstSeq2.add(seq2);
    lstSeq2.add(seq1);
    
    final int numValues = 10000;

    CounterTask ct1 = new CounterTask(lstSeq1, numValues);
    CounterTask ct2 = new CounterTask(lstSeq2, numValues);

    ct1.start();
    ct2.start();

    

    System.out.println(seq1.getAndIncrement(seq2));
    System.out.println(seq2.getAndIncrement(seq1));
  }
}
