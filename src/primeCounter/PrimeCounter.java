package primeCounter;

import java.util.concurrent.ExecutionException;

public class PrimeCounter {

	private final static int LOWER = 1;
	private final static int UPPER = 1000000;
	private final static int PARTITIONS = 100;
	
//	public static void main(String[] args)
//		throws InterruptedException, ExecutionException {
//		
//		AbstractPrimeFinder finder = new SequentialPrimeFinder();
//		
//		long start = System.nanoTime() ;
//		int primeCount = finder.getPrimesInRange(LOWER, UPPER);
//		long end = System.nanoTime();
//		
//		System.out.printf("Number of primes in range [%,d,%,d] = %,d\n",
//				LOWER, UPPER, primeCount);
//		System.out.printf("Calculation took %,.2f seconds\n", (end - start)/1.09e9);
//	}
	
	public static void main(String[] args)
			throws InterruptedException, ExecutionException {
			
			int poolSize = Runtime.getRuntime().availableProcessors();
			AbstractPrimeFinder finder = new ConcurrentPrimeFinder(poolSize, PARTITIONS);
			
			long start = System.nanoTime() ;
			int primeCount = finder.getPrimesInRange(LOWER, UPPER);
			long end = System.nanoTime();
			
			System.out.printf("Number of primes in range [%,d,%,d] = %,d\n",
					LOWER, UPPER, primeCount);
			System.out.printf("Calculation took %,.2f seconds\n", (end - start)/1.09e9);
		}
	
	
}
