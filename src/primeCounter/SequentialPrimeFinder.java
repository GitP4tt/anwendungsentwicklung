package primeCounter;

public class SequentialPrimeFinder extends AbstractPrimeFinder {
	
	@Override
	public int getPrimesInRange(int lower, int upper){
		
		return countPrimesInRange(lower, upper);
	}
}
