package answerToEverything;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

public class AnswerToEverything {

  private final JFrame frame = new JFrame("Find the Answer");
  private final JLabel label = new JLabel();
  private final JButton button = new JButton("Find it");


  private String findAnswerToEverything() {

    System.out.println("findAnswerToEverything: " + Thread.currentThread());

    try {
      Thread.sleep(10000);
    }
    catch (InterruptedException e) {
      e.printStackTrace();
    }

    return "42";
  }


  public AnswerToEverything() {

    System.out.println("AnswerToEverything: " + Thread.currentThread());

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(320, 80);
  
    frame.getContentPane().add(label, BorderLayout.WEST);
    frame.getContentPane().add(button, BorderLayout.EAST);
    label.setText("What is the answer to everything?");

    button.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent event) {

        System.out.println("actionPerformed(): " + Thread.currentThread());
        label.setText(findAnswerToEverything());
        //new AnswerToEverythingFinder().execute();
      }
    });

    frame.setVisible(true);
  }


  public static void main(String[] args) {

    System.out.println("main: " + Thread.currentThread());

    // Start Swing GUI in the Event Dispatch Thread
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {

        new AnswerToEverything();
      }
    });
  }


  private class AnswerToEverythingFinder extends SwingWorker<String, Object> {

    @Override
    public String doInBackground() {

      // doInBackground() is executed in a thread of its own
      return findAnswerToEverything();
    }


    @Override
    protected void done() {

      // done() is called in the Event Dispatch Thread after
      // doInBackground is finished
      try {

        System.out.println("done: " + Thread.currentThread());
        // get() retrieves the result returned by doInBackground()
        label.setText(get());

      }
      catch (InterruptedException | ExecutionException e) {

        e.printStackTrace();
      }
    }
  }

}
