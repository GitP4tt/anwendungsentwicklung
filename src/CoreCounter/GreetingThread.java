package CoreCounter;
import java.time.*;
import java.time.temporal.*;
import java.util.*;


public class GreetingThread extends Thread {
	
	private int iterations;
	private int delay;
	
	public GreetingThread(int iterations, int delay) {
		this.iterations = iterations;
		this.delay = delay ;
	}
	
	public void run() {
		for( int i = 0; i < iterations; i++){
			LocalTime now = LocalTime.now().truncatedTo(ChronoUnit.SECONDS);
			System.out.println(now + ": Hello, World!");
			try{
				Thread.sleep(delay);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

}
