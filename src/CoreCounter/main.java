package CoreCounter;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class main {
	private final static int ITERATIONS = 10;
	private final static int DELAY = 1000;
	
	public static void main(String[] args) {
		
		Thread t1 = new GreetingThread(ITERATIONS, DELAY);
		Runnable r = new SayGoodbyeRunnable(ITERATIONS, DELAY);
		Thread t2 = new Thread(r);
		
		t1.start();
		t2.start();
	
	}
}