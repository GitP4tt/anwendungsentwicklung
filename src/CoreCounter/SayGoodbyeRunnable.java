package CoreCounter;
import java.time.LocalTime;
import java.time.temporal.*;

public class SayGoodbyeRunnable implements Runnable{
	private int iterations;
	private int delay;
	
	public SayGoodbyeRunnable(int iterations, int delay) {
		this.iterations = iterations;
		this.delay = delay ;
	}
	
	public void run() {
		for( int i = 0; i < iterations; i++){
			LocalTime now = LocalTime.now().truncatedTo(ChronoUnit.SECONDS);
			System.out.println(now + ": Bye, World!");
			try{
				Thread.sleep(delay);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

}
