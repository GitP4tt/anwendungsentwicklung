package oddEven;


import java.util.concurrent.Callable;

public abstract class AbstractNumberPrinter implements Runnable {
    private int limit;
    private Object lockme ;

    AbstractNumberPrinter(int limit, Object lockme){
        this.limit = limit;
       this.lockme = lockme;
    }

    public void run(){
    		synchronized(lockme){
    			for (int i=0; i<= limit; i++){
                    if(numberAccepted(i)){
                        try{
                            Thread.sleep(500);
                        }
                        catch (InterruptedException e){
                            e.printStackTrace();
                        }
                        System.out.println(i);
                    }
                }
    		}
    			
    		
    		
    	}
    		
    	
        
    

    protected abstract boolean numberAccepted(int i);
}
