package oddEven;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class NumberPrinterRunner {
	public static void main(String[] args) throws InterruptedException {
		int limit = 20;
		
		
		
		Object lockObject = new Object();
		
		
		Thread t1 = new Thread( new EvenNumberPrinter(limit,lockObject) );

		Thread t2 = new Thread(new OddNumberPrinter(limit,lockObject));
		
		t2.start();
		t1.start();
		
			
		
		

		// try {
		// t1.join();
		// }
		// catch (InterruptedException e){
		// e.printStackTrace();
		// }

	}
}
