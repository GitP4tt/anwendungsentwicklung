package oddEven;


public class EvenNumberPrinter extends AbstractNumberPrinter {
    EvenNumberPrinter(int limit,Object lockme){
        super(limit, lockme);
    }

    @Override protected boolean numberAccepted(int i){
        return (i % 2 ) == 0;
    }

}
