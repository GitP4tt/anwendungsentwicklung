package counter;

public class SafeSequence implements Sequence{

	private int value = 0;
	
	@Override
	public synchronized int getAndIncrement() {
		
		return value++;
	}
	

}
