package counter;

import java.util.concurrent.locks.*;

public class SafeSequenceWithLock implements Sequence {

	private Lock l1 = new ReentrantLock();

	private int value = 0;

	@Override
	public int getAndIncrement() {

		int current; //Damit kein doppel increment vor dem Return erfolgen kann
		
		l1.lock();
		System.out.println("value = " + getCurrent());
		current = value++;
		l1.unlock();
		
		//sonst würde hier eventuell ein anderer Thread erhöhen
		return current;

	}

	private int getCurrent() {

		return value;
	}

}
