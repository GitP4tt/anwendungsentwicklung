package counter;

public class Counter {

	public static void main(String[] args){
		
		
		final Sequence sequence = new SafeSequence();
		final int numValues = 10000000;
		
		
		CounterTask ct1 = new CounterTask(sequence, numValues);
		CounterTask ct2 = new CounterTask(sequence, numValues);
		
		ct1.start();
		ct2.start();
		
		try {
			ct1.join();
			
			
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(sequence.getAndIncrement());
	}
}
