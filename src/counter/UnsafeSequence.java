package counter;

public class UnsafeSequence implements Sequence {
	
	private int value = 0 ;
	
	@Override
	public int getAndIncrement(){
		return value++;
	}
	
	

}
