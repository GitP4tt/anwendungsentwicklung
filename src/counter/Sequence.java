package counter;

public interface Sequence {

	int getAndIncrement();
	
	
}
