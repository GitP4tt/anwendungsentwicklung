package NoVisibility;

public class NoVisibilty {

	public static volatile boolean done = false ;
	public static volatile int value = 0;
	
	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		
		new Thread(new Runnable(){
			public void run () {
				while(!done){
					
					System.out.println("läuft..");
					;
				}
				System.out.println("value = " + value);
			}
		}).start();
		
		
		Thread.sleep(500);
		
		value = 42 ;
		done = true;
		
		
		
		System.out.println("Flag \"done\" is true");

	}

}
