package hollywood;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.Actors;
import akka.actor.UntypedActorFactory;

public class HollywoodManager {

	public static void main(String[] args) throws InterruptedException {
		ActorRef movieActor = Actors.actorOf(new UntypedActorFactory() {
			@Override
			public Actor create() {
				return new HollywoodActor("Johnny Depp");
			}
		}).start();

		movieActor.sendOneWay("Jack Sparrow");
		Thread.sleep(100);
		movieActor.sendOneWay("Edward Scissorshands");
		Thread.sleep(100);
		movieActor.sendOneWay("Willy Wonka");
		Thread.sleep(100);
		movieActor.sendOneWay(new StringBuilder("Willy Wonka"));

		Actors.registry().shutdownAll();
	}
}
